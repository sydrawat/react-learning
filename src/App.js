import React, { Component } from 'react';
import classes from './App.module.css';
import Person from './Person/Person';
import ErrorBoundary from './ErrorBoundary/ErrorBoundary'

class App extends Component {
  state = {
    //state which has property persons
    persons: [
      { id: 'f394h', name: 'sid', age: 24 },
      { id: 'nfo348', name: 'max', age: 28 },
      { id: '384gb', name: 'manu', age: 26 },
    ],
    otherState: 'some other value',
    showPersons: false
  }

  //event handler function
  deletePersonHandler = (personIndex) => {
    // const persons = this.state.persons.slice();
    // mutated the data using splice()
    //creates a copy using spread operator and then perform changes
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({
      persons: persons
    });
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex((prsn) => {
      return prsn.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons: persons })
  }

  togglePersonsHandler = (event) => {
    const doesShow = this.state.showPersons;
    this.setState({
      showPersons: !doesShow
    });
  }
  render() {

    let persons = null;
    let btnClass = '';

    if (this.state.showPersons) {
      // JSX element
      persons = (
        < div>
          {this.state.persons.map((person, index) => {
            return (
              <ErrorBoundary key={person.id}>
                <Person
                  _onClick={this.deletePersonHandler.bind(this, index)}
                  name={person.name}
                  age={person.age}
                  _onChange={(event) => this.nameChangeHandler(event, person.id)} />
              </ErrorBoundary>
            );
          })}
        </div>
      );

      btnClass = classes.Red;
    }
    //set css classNames dynamically
    // const cssClass = ['red', 'bold'].join(' ');// "red bold"
    const assignedClass = [];
    if (this.state.persons.length <= 2) {
      assignedClass.push(classes.red); // cssClass = ['red']
    }
    if (this.state.persons.length <= 1) {
      assignedClass.push(classes.bold);// cssClass = ['red', 'bold']
    }
    return (
      <div className={classes.App}>
        <h1>Hi, I'm a react App!</h1>
        <p className={assignedClass.join(' ')}>This is really working!</p>
        <button
          className={btnClass}
          onClick={this.togglePersonsHandler}>Toggle Persons</button>
        {persons}
      </div>
    );
    // return React.createElement('div', null, React.createElement('h1', null, 'Is this working now??'));

    //To get the App.css style working, remove null
    //Add {className: 'App'}
    // return React.createElement('div', { className: 'App' }, React.createElement('h1', null, 'Is this working now??'));

    //Really cumbersome to write the above code : React.createElement.
    //Becomes difficult to write if we have more nested components.
    //Hence we prefer JSX.
  }
}

export default App;
