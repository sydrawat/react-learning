import React from 'react';
import classes from './Person.module.css'

const person = (props) => {
    // const err = Math.random();
    // if (err > 0.7) {
    //     throw new Error('something went wrong!');
    // }
    return (
        <div className={classes.Person}>
            <p onClick={props._onClick}>I'm {props.name} and I am {props.age} years old!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props._onChange} value={props.name} />
        </div>
    );
};

export default person;